#  Tryptex/Gamegenorator/Pierce's Desktop Wallpapers

This repo isn't all that special, it's primary purpose is to simply be a library of desktop wallpapers that I encounter and take a liking to. Feel free to clone this repo and use it as a starting place for your own digital archive for Wallpapers (Assuming you don't have one already).

![](demo2.png)

## Dynamic Wallpapers (Linux Only)

To use the dynamic wallpapers, you need to change the directories in the .xml file to match your username and clone directory (I recommend using Replace All in whatever editor you using =D). (I am considering making `/usr/share/backgrounds/` the default directory and recommended place to clone but I'm still considering it and would appreciate feedback regarding it).

Then you can set the wallpaper to the .xml file in Gnome tweaks (Only tested on Gnome, I intend to run tests on other DE's soon but I know Budgie does not currently support .xml files). I use [Dynamic Wallpaper Editor](https://github.com/maoschanz/dynamic-wallpaper-editor) to create and edit the .xml files.

***

This project is inspired by DistroTube's vast collection of wallpapers over at https://gitlab.com/dwt1/wallpapers